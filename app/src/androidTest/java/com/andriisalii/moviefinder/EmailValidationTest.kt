package com.andriisalii.moviefinder


import androidx.test.ext.junit.runners.AndroidJUnit4
import com.andriisalii.moviefinder.util.ValidationUtils
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EmailValidationTest {

    @Test
    fun email_contain_two_at_together() {
        assertFalse(ValidationUtils.isEmailValid("email@@email.com"))
    }

    @Test
    fun email_contain_two_at_not_together() {
        assertFalse(ValidationUtils.isEmailValid("email@separator@email.com"))
    }

    @Test
    fun email_contain_two_dot_together() {
        assertFalse(ValidationUtils.isEmailValid("email@email..com"))
    }
    @Test
    fun email_contain_dot_in_postfix(){
        assertTrue(ValidationUtils.isEmailValid("email@emai.l.com"))
    }

    @Test
    fun email_contain_too_short_postfix() {
        assertFalse(ValidationUtils.isEmailValid("email@.com"))
    }
    @Test
    fun email_contain_only_numbers_in_postfix() {
        assertTrue(ValidationUtils.isEmailValid("email@1.com"))
        assertTrue(ValidationUtils.isEmailValid("email@123.com"))
    }

    @Test
    fun email_contain_too_short_prefix() {
        assertFalse(ValidationUtils.isEmailValid("@email.com"))
    }

    @Test
    fun email_contain_too_short_domen() {
        assertFalse(ValidationUtils.isEmailValid("email@separator@email."))
        assertFalse(ValidationUtils.isEmailValid("email@separator@email.c"))

    }

    @Test
    fun email_contain_too_long_domen() {
        assertFalse(ValidationUtils.isEmailValid("email@separator@email.comfdasfadsf"))
    }

    @Test
    fun email_contain_domen_with_numbers() {
        assertFalse(ValidationUtils.isEmailValid("email@separator@email.co12"))
    }

}