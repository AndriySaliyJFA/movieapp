package com.andriisalii.moviefinder

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import com.andriisalii.moviefinder.loginScreen.LoginActivity
import com.andriisalii.moviefinder.util.firebase.FirebaseManagerImpl
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var firebaseManager: FirebaseManagerImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firebaseManager = FirebaseManagerImpl.getInstance(baseContext)

        initalizeToolbar()
        initalizeBottomNavigation()
        initializeNavigationDrawer()
    }

    private fun initalizeToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
    }

    private fun initalizeBottomNavigation() {
        bottomNavigation.selectedItemId = R.id.item_discover
        //show badge without number(just red point)
        bottomNavigation.getOrCreateBadge(R.id.item_watchlist)

        bottomNavigation.setOnNavigationItemSelectedListener { item: MenuItem ->
            return@setOnNavigationItemSelectedListener when (item.itemId) {
                R.id.item_for_me -> true
                R.id.item_discover -> true
                R.id.item_watchlist -> true
                else -> false
            }
        }
    }

    private fun initializeNavigationDrawer() {
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.app_name, R.string.app_name
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener(this)
        navigationView.bringToFront()
    }

    override fun onBackPressed() {
        MaterialAlertDialogBuilder(this)
            .setTitle("Do you want to close app?")
            .setPositiveButton("Cancel", null)
            .setNegativeButton("Yes,exit") { _, _ -> finish() }
            .show()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_sign_out -> {
                firebaseManager.signOut().subscribe(
                    {
                        startActivity(Intent(baseContext, LoginActivity::class.java))
                        finish()
                    },
                    { showError(R.string.err_logout) }
                ).dispose()
                true
            }
            else -> false
        }
    }

    private fun showError(stringResId: Int? = null, stringValue: String = "") {
        Snackbar.make(
            coordinatorMain, if (stringResId == null) {
                stringValue
            } else {
                getString(stringResId)
            }, Snackbar.LENGTH_SHORT
        )
            .show()
    }
}