package com.andriisalii.moviefinder

import android.app.Application
import android.content.Context
import com.google.firebase.auth.FirebaseAuth

/**
 * Main reason for this class is that it contains context to receive strings by ID
 * it helps to avoid writing separate BindAdapter for each String-bonded field
 *
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
    }

    companion object {
        private lateinit var instance: App
        lateinit var context: Context
            private set
    }
}