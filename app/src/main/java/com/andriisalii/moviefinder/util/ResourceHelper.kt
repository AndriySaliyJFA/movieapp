package com.andriisalii.moviefinder.util

import com.andriisalii.moviefinder.App

class ResourceHelper{
    companion object{
        fun getString(stringId:Int):String{
            return App.context.resources.getString(stringId)
        }
    }
}