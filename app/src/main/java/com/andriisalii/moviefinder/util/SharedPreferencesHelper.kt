package com.andriisalii.moviefinder.util

import android.content.Context
import android.content.SharedPreferences
import com.andriisalii.moviefinder.R

class SharedPreferencesHelper private constructor(context: Context) {
    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(
        context.getString(R.string.shared_preferences), 0)

    fun isFirstRun(): Boolean {
        return readData(KEY_FIRST_RUN) == "false"
    }

    fun writeData(key: String, data: String) {
        sharedPreferences.edit().putString(key, data).apply()
    }

    fun readData(key: String): String? {
        return sharedPreferences.getString(key, "false")
    }

    companion object :
        SingletonHolder<SharedPreferencesHelper, Context>(::SharedPreferencesHelper) {
        var KEY_FIRST_RUN = "firs_run"
    }
}