package com.andriisalii.moviefinder.util.firebase

import android.content.Context
import com.andriisalii.moviefinder.util.SingletonHolder
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Completable

class FirebaseManagerImpl private constructor(_context: Context) : IFirebaseManager {
    companion object :
        SingletonHolder<FirebaseManagerImpl, Context>(::FirebaseManagerImpl)

    private var firebaseAuth = FirebaseAuth.getInstance()
    private var context = _context
    override fun signOut(): Completable =
        Completable.create { emitter ->
            firebaseAuth.signOut()
            GoogleSignIn.getClient(context, GoogleSignInOptions.DEFAULT_SIGN_IN).signOut()
                .addOnFailureListener { emitter.onError(it) }
            emitter.onComplete()
        }

    override fun isUserGuest(): Boolean {
        return firebaseAuth.currentUser!!.isAnonymous
    }

    override fun isUserLoggined(): Boolean {
        return firebaseAuth.currentUser != null
    }
}