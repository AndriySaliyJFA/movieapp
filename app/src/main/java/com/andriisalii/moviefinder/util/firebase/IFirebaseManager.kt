package com.andriisalii.moviefinder.util.firebase

import io.reactivex.Completable

interface IFirebaseManager {

    fun signOut(): Completable

    fun isUserGuest(): Boolean

    fun isUserLoggined(): Boolean
}