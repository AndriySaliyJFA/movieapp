package com.andriisalii.moviefinder.signUpScreen.model

data class LoginErrorFields(
    var emailError: String?, var passwordError: String?
) {
    constructor() : this(null, null)
}
