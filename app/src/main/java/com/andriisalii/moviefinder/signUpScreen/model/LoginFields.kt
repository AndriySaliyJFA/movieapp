package com.andriisalii.moviefinder.signUpScreen.model

data class LoginFields(var email: String, var password: String) {
    constructor() : this("", "")
}