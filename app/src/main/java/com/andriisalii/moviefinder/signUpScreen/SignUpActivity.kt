package com.andriisalii.moviefinder.signUpScreen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.andriisalii.moviefinder.R
import com.andriisalii.moviefinder.databinding.ActivitySignUpBinding
import com.andriisalii.moviefinder.loginScreen.LoginFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var mLoginViewModel: SignUpViewModel
    private lateinit var mBinding: ActivitySignUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        val launchState = LoginFragment.LaunchStates.values()[
                intent.getIntExtra(
                    LoginFragment.KEY_STATE,
                    LoginFragment.LaunchStates.LAUNCH_FOR_REGISTRATION.ordinal
                )]

        setupBindings()
        prepareUI(launchState)
    }

    private fun prepareUI(launchState: LoginFragment.LaunchStates) {
        when (launchState) {
            LoginFragment.LaunchStates.LAUNCH_FOR_LOGIN -> {
                btnCreateAcc.setText(R.string.login_email)
                btnCreateAcc.setOnClickListener {
                    mLoginViewModel.onLoginButtonClicked()
                }
            }
            LoginFragment.LaunchStates.LAUNCH_FOR_REGISTRATION -> {
                btnCreateAcc.setText(R.string.action_sign_up)
                btnCreateAcc.setOnClickListener {
                    mLoginViewModel.onCreateButtonClicked()

                }
            }
            else -> showError(R.string.err_login)
        }

    }

    private fun showError(stringResId: Int? = null, stringValue: String = "") {
        Snackbar.make(
            scrollViewSignUp, if (stringResId == null) {
                stringValue
            } else {
                getString(stringResId)
            }, Snackbar.LENGTH_SHORT
        )
            .show()
    }

    private fun setupBindings() {
        mLoginViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        mBinding.viewModel = mLoginViewModel
        mLoginViewModel.isSignInSuccessful.observe(this, Observer<Boolean> { isSuccessful ->
            if (isSuccessful) {
                finish()
            }
        }
        )

        mLoginViewModel.errorMessage.observe(this, Observer<String> { errorMsg ->
            showError(stringValue = errorMsg)
        })
    }
}