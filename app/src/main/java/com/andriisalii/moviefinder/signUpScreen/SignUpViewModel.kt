package com.andriisalii.moviefinder.signUpScreen

import android.app.Application
import android.util.Log
import android.view.View.OnFocusChangeListener
import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import com.andriisalii.moviefinder.signUpScreen.model.LoginForm
import com.andriisalii.moviefinder.util.SingleLiveEvent
import com.google.firebase.auth.FirebaseAuth


class SignUpViewModel(application: Application) : AndroidViewModel(application) {

    private val TAG = javaClass.simpleName

    val isSignInSuccessful = SingleLiveEvent<Boolean>()
    val errorMessage = SingleLiveEvent<String>()
    val loginForm: LoginForm = LoginForm()
    var onEmailFocus: OnFocusChangeListener
    var onPasswordFocus: OnFocusChangeListener
    private var firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    //region Binding adapters
    object OnFocusBindingAdapter {
        @BindingAdapter("onFocus")
        @JvmStatic
        fun bindFocusChange(
            editText: EditText,
            onFocusChangeListener: OnFocusChangeListener?
        ) {
            if (editText.onFocusChangeListener == null) {
                editText.onFocusChangeListener = onFocusChangeListener
            }
        }
    }
    //endregion

    init {
        onEmailFocus = OnFocusChangeListener { view, focused ->
            val et = view as EditText
            if (focused) {
                Log.d(TAG, "Focus changed to email")
                loginForm.clearEmailError()
            } else {
                Log.d(TAG, "Focus changed from email")
                loginForm.checkEmail(et.text.isNotEmpty())
            }
        }

        onPasswordFocus = OnFocusChangeListener { view, focused ->
            val et = view as EditText
            if (focused) {
                Log.d(TAG, "Focus changed to password")
                loginForm.clearPasswordError()
            } else {
                Log.d(TAG, "Focus changed from password")
                loginForm.checkPassword(et.text.isNotEmpty())
            }
        }
    }

    fun onCreateButtonClicked() {
        firebaseAuth.createUserWithEmailAndPassword(
            loginForm.loginFields.email,
            loginForm.loginFields.password
        ).addOnCompleteListener { task ->
            isSignInSuccessful.value = task.isSuccessful
        }.addOnFailureListener { exception ->
            errorMessage.value = exception.localizedMessage
        }
    }

    fun onLoginButtonClicked() {
        firebaseAuth.signInWithEmailAndPassword(
            loginForm.loginFields.email,
            loginForm.loginFields.password
        ).addOnCompleteListener { task ->
            isSignInSuccessful.value = task.isSuccessful
        }.addOnFailureListener { exception ->
            errorMessage.value = exception.localizedMessage
        }
    }
}