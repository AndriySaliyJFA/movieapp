package com.andriisalii.moviefinder.loginScreen


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.andriisalii.moviefinder.MainActivity
import com.andriisalii.moviefinder.R
import com.andriisalii.moviefinder.signUpScreen.SignUpActivity
import com.andriisalii.moviefinder.util.SharedPreferencesHelper
import com.andriisalii.moviefinder.util.firebase.FirebaseManagerImpl
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    private val TAG = javaClass.simpleName

    companion object {
        val KEY_STATE: String = "isLogined"
    }

    enum class LaunchStates {
        LAUNCH_DEFAULT,
        LAUNCH_AS_GUEST,
        LAUNCH_FOR_LOGIN,
        LAUNCH_FOR_LOGINED,
        LAUNCH_FOR_REGISTRATION
    }

    private val REC_CODE_GOOGLE = 1
    private val REC_CODE_CREATE = 2
    private lateinit var spHelper: SharedPreferencesHelper
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var firebaseManagerImpl: FirebaseManagerImpl

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        spHelper = SharedPreferencesHelper.getInstance(context!!)
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseManagerImpl = FirebaseManagerImpl.getInstance(context!!)
        //region pre-Start checks
        if (spHelper.isFirstRun()) {
            spHelper.writeData(SharedPreferencesHelper.KEY_FIRST_RUN, "true")
            handleLaunchState(LaunchStates.LAUNCH_DEFAULT)
        } else {
            if (firebaseManagerImpl.isUserLoggined()) {
                if (firebaseManagerImpl.isUserGuest()) {
                    handleLaunchState(LaunchStates.LAUNCH_AS_GUEST)
                } else {
                    handleLaunchState(LaunchStates.LAUNCH_FOR_LOGINED)
                }
            } else {
                handleLaunchState(LaunchStates.LAUNCH_DEFAULT)
            }
        }
        //endregion
        //region OnClickListeners
        btnSignUp.setOnClickListener {
            startSignUpActivity(LaunchStates.LAUNCH_FOR_REGISTRATION)
        }
        btnLoginWithEmail.setOnClickListener {
            startSignUpActivity(LaunchStates.LAUNCH_FOR_LOGIN)
        }
        btnLoginWithGoogle.setOnClickListener {
            onGoogleButtonClick()
        }
        tvSkip.setOnClickListener {
            loginAnonymously()
        }
        //endregion
    }

    private fun handleLaunchState(state: LaunchStates) {
        when (state) {
            LaunchStates.LAUNCH_DEFAULT -> {
                tvSkip.visibility = View.VISIBLE
            }
            LaunchStates.LAUNCH_AS_GUEST -> {
                startMainActivity(LaunchStates.LAUNCH_AS_GUEST)
            }
            LaunchStates.LAUNCH_FOR_LOGIN -> {
                tvSkip.visibility = View.INVISIBLE
            }
            LaunchStates.LAUNCH_FOR_LOGINED -> startMainActivity(LaunchStates.LAUNCH_FOR_LOGINED)
        }
    }

    private fun onGoogleButtonClick() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(context!!, gso)
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, REC_CODE_GOOGLE)
    }

    private fun startMainActivity(state: LaunchStates) {
        val bundle = Bundle()
        bundle.putInt(KEY_STATE, state.ordinal)
        startActivity(Intent(context, MainActivity::class.java).putExtras(bundle))
        activity!!.finish()
    }

    private fun startSignUpActivity(state: LaunchStates) {
        val bundle = Bundle()
        bundle.putInt(KEY_STATE, state.ordinal)
        startActivityForResult(
            Intent(context, SignUpActivity::class.java)
                .putExtras(bundle), REC_CODE_CREATE
        )
    }

    private fun loginAnonymously() {
        firebaseAuth.signInAnonymously()
            .addOnCompleteListener(activity as Activity) { task ->
                if (task.isSuccessful) {
                    startMainActivity(LaunchStates.LAUNCH_AS_GUEST)
                } else {
                    showError(R.string.err_login)
                }
            }
    }

    private fun showError(stringResId: Int) {
        Snackbar.make(loginContainer, stringResId, Snackbar.LENGTH_SHORT)
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REC_CODE_GOOGLE -> {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    val credential = GoogleAuthProvider.getCredential(account!!.idToken, null)
                    FirebaseAuth.getInstance().signInWithCredential(credential)
                    startMainActivity(LaunchStates.LAUNCH_AS_GUEST)
                } catch (e: ApiException) {
                    showError(R.string.err_login)
                }
            }
            REC_CODE_CREATE -> {
                if (firebaseManagerImpl.isUserLoggined()) {
                    startMainActivity(LaunchStates.LAUNCH_AS_GUEST)
                }
            }
        }
    }
}