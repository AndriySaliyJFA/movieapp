package com.andriisalii.moviefinder.loginScreen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.andriisalii.moviefinder.R

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        val fragment: Fragment? = supportFragmentManager.findFragmentByTag("uniqueTag")

        if (fragment == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.container_login, LoginFragment(), "uniqueTag")
                .commit()
        } else { // re-use the old fragment
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_login, fragment, "uniqueTag")
                .commit()
        }
    }
}